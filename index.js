const express = require("express");
const app = express();

const bodyParser = require('body-parser');
const { mongoose } = require("./db/db");

const userController = require("./controllers/userController");



// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json())


app.use("/user", userController);

app.listen(3333, () => {
    console.log("server is running :)");
});